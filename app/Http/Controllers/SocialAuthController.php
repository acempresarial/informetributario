<?php

namespace acempresarial\Http\Controllers;

use Illuminate\Http\Request;

use acempresarial\Http\Requests;
use acempresarial\Http\Controllers\Controller;
use acempresarial\SocialAccountService;
use Socialite;

class SocialAuthController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function callback(SocialAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('facebook')->user());

        auth()->login($user);

        return redirect()->to('/home');
    }
}