<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => 'guest'], function(){
     	Route::get('/', function(){
			return redirect('/login');
		});
});

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
|
*/
Route::auth();
Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');



Route::post('/users/{id}/ctes', 'CTEsController@upload');
Route::post('/report/generate', 'ReportController@generate');

Route::group(['middleware' => ['auth']
    ], function () {
    	Route::get('/ctes/upload', 'CTEsController@uploader');
    	Route::get('/cte/{id}/wizard', 'CTEsController@EconomicActivitiesWizard');
 		Route::resource('cte', 'CTEsController');
 		Route::get('/cte/{id}/reports', 'ReportController@index'); 		
 		Route::get('/reports/{id}', 'ReportController@show');
 		Route::get('api/reports/{id}', 'api\ReportAPI@show');

    });


