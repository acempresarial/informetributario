<?php

namespace acempresarial\Models;

use Illuminate\Database\Eloquent\Model;

class EconomicSector extends Model
{

	protected $guarded = [
        'id'
    ];
     /**
     * Get the comments for the blog post.
     */
    public function economicActivities()
    {
        return $this->hasMany('acempresarial\Models\EconomicActivity');
    }
}
