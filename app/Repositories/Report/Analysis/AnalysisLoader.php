<?php

namespace acempresarial\Repositories\Report\Analysis;

use acempresarial\Repositories\Report\Analysis\GeneralInformation\GeneralInformationLoader;
use acempresarial\Repositories\Report\Analysis\Business\BusinessLoader;
use acempresarial\Repositories\Report\Analysis\BenchMark\BenchMarkLoader;
use acempresarial\Repositories\Report\Analysis\Operations\OperationsLoader;
use acempresarial\Repositories\Report\Analysis\HHRR\HHRRLoader;
use acempresarial\Repositories\Report\Analysis\Financial\FinancialLoader;
use acempresarial\Repositories\Report\Analysis\Acceptance\AcceptanceLoader;
use acempresarial\Models\Cte;

class AnalysisLoader
{
	public function load($wizard)
	{

		$analysis = array();
		$error = array();
		$cte = Cte::findOrFail($wizard['cte_id']);
		$analysis['CTE']= $cte;

		$analysis['CTE']['Acceptance'] = (new AcceptanceLoader)->get($cte,$wizard);

		if($analysis['CTE']['Acceptance']['State'] == false)
		{
				
			$error['state'] = false;
			$error['messages']=$analysis['CTE']['Acceptance'] ;
			return $error;
		}

		$analysis['GeneralInformation'] = (new GeneralInformationLoader)->get($cte,$wizard);
		$analysis['Scopes'] = (new GeneralInformationLoader)->get($cte,$wizard);	
		$analysis['Business'] = (new BusinessLoader)->get($cte);		
		$analysis['Operations'] = (new OperationsLoader)->get($cte);
		$analysis['HHRR'] = (new HHRRLoader)->get($cte);
		$analysis['Financial'] = (new FinancialLoader)->get($cte,$analysis);		
		$analysis['Benchmark'] = (new BenchMarkLoader)->get($cte ,$analysis);
	
		return $analysis;
	}
}