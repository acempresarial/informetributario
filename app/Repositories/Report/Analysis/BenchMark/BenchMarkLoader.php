<?php 
namespace acempresarial\Repositories\Report\Analysis\BenchMark;
use acempresarial\Repositories\Report\Analysis\BenchMark\SalesBenchmark;
use acempresarial\Repositories\Report\Analysis\BenchMark\ReturnOnSalesBenchMark;
use acempresarial\Repositories\Report\Analysis\BenchMark\ReturnOnAssetsBenchMark;
use acempresarial\Repositories\Report\Analysis\BenchMark\RemunerationsBenchMark;
use acempresarial\Repositories\Report\Analysis\BenchMark\ProductivityBenchMark;
use acempresarial\Repositories\Report\Analysis\BenchMark\CompetitivenessSummary;


use DB;
/**
* 
*/
class BenchMarkLoader 
{
	private $report;
	private $cte;
	public function get($cte ,$report)
	{
		$this->report = $report;
		$this->cte = $cte;			
		return $this->handle();
	}

	private function handle()
	{
		//Relevant Information for the calculations
		$Information = 
		[
			'segment' => $this->report['GeneralInformation']['segment']['value'],
			'sector' => $this->report['GeneralInformation']['sector']['code'],
			'UFfactor' => $this->report['GeneralInformation']["UfFactor"],
			'cte' => $this->cte,
			'report' =>$this->report
		];

		//Load the BenchMark from the DB
		$benchmarkDB = collect(DB::table('benchmarks')->where('economic_sector_code','=', $Information['sector'])
		->where('segment_value','=',$Information['segment'])->first())->toArray();		
		
		//Load the Calculations from their respective classes.
		$benchmark['Sales']= (new SalesBenchmark)->get($Information, $benchmarkDB);
		$benchmark['ReturnOnSales']= (new ReturnOnSalesBenchMark)->get($Information, $benchmarkDB);
		$benchmark['ReturnOnAssets']= (new ReturnOnAssetsBenchMark)->get($Information, $benchmarkDB);
		$benchmark['Remunerations']= (new RemunerationsBenchMark)->get($Information, $benchmarkDB);
		$benchmark['Productivity']= (new ProductivityBenchMark)->get($Information, $benchmarkDB);
		$benchmark['Competitiveness']= (new CompetitivenessSummary)->get($benchmark);

		
		return $benchmark;
	}
	
}