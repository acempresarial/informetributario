<?php

namespace acempresarial\Repositories\Report\Analysis\BenchMark;

class CompetitivenessSummary
{
    
    private $benchmark;
    public function get($benchmark)
    {
        $this->benchmark = $benchmark;        
        return $this->recipe();
    }
    
    
    private function recipe()
    {
        $competitiveness = [];
        $competitiveness['labels'] = 
        [
            'Ventas',
            'Remuneraciones',
            'Productividad',
            'Rentabilidad sobre las Ventas',
            'Rentabilidad sobre los Activos'
        ];

       
        $salesMax = $this->benchmark['Sales']['max'];

        if($this->benchmark['Sales']['company'] > $this->benchmark['Sales']['max'] )
            {
                $salesMax = $this->benchmark['Sales']['company'];
            }

        $remMax =  $this->benchmark['Remunerations']['max'];

        if($this->benchmark['Remunerations']['company'] > $this->benchmark['Remunerations']['max'] )
            {
                $remMax = $this->benchmark['Remunerations']['company'];
            }

        $prodMax = $this->benchmark['Productivity']['max'];

       if($this->benchmark['Productivity']['company'] > $this->benchmark['Productivity']['max'] )
        {
            $prodMax  = $this->benchmark['Productivity']['company'];
        }


        $ReturnSalesMax = 0;
        if(isset($this->benchmark['ReturnOnSales']['max'])&& $this->benchmark['ReturnOnSales']['max']!= 0)
            {
               $ReturnSalesMax =  (100/$this->benchmark['ReturnOnSales']['max'])*
                $this->benchmark['ReturnOnSales']['max'];
            }


         $ReturnAssetsMax = 0;   
          if(isset($this->benchmark['ReturnOnAssets']['max'])&& $this->benchmark['ReturnOnAssets']['max']!= 0)
            {
              $ReturnAssetsMax  =  (100/$this->benchmark['ReturnOnAssets']['max'])*
            $this->benchmark['ReturnOnAssets']['max'];
            }

        $competitiveness['max'] = 
        [


            (100/$salesMax) * $salesMax ,
            (100/$remMax) * $remMax,
            (100/$prodMax) * $prodMax,
                $ReturnSalesMax,
                $ReturnAssetsMax            
        ];

        $competitiveness['min'] = 
        [
            $this->benchmark['Sales']['min'],
            $this->benchmark['Remunerations']['min'],
            $this->benchmark['Productivity']['min'],
            $this->benchmark['ReturnOnSales']['min'],
            $this->benchmark['ReturnOnAssets']['min']
        ];

         $competitiveness['avg'] = 
        [
           100 * ($this->benchmark['Sales']['avg']/$salesMax),
           100 * ($this->benchmark['Remunerations']['avg']/$remMax),
           100 * ($this->benchmark['Productivity']['avg']/$prodMax),
           100 * (
                $this->benchmark['ReturnOnSales']['avg']/
                $this->benchmark['ReturnOnSales']['max']
            )
           ,
           100 * (
                $this->benchmark['ReturnOnAssets']['avg']/
                $this->benchmark['ReturnOnAssets']['max']
            )
            
        ];
        $competitiveness['company'] = 
        [
            100 * ($this->benchmark['Sales']['company'] / $salesMax),
            100 * ($this->benchmark['Remunerations']['company'] / $remMax),
            100 * ($this->benchmark['Productivity']['company'] / $prodMax),
            100 * (
                    $this->benchmark['ReturnOnSales']['company']/
                    $this->benchmark['ReturnOnSales']['max']
                    )
                ,
           100 * (
                 $this->benchmark['ReturnOnAssets']['company']/
                 $this->benchmark['ReturnOnAssets']['max']
                )
            
        ];


        $competitiveness = collect($competitiveness);
        return $competitiveness   ;        
    
        
    }
}