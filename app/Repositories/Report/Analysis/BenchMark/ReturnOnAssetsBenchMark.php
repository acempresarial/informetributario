<?php

namespace acempresarial\Repositories\Report\Analysis\BenchMark;
use acempresarial\Helpers\PHPhelpers;

class ReturnOnAssetsBenchMark
{
    private $Information;
    private $benchmark;
    public function get($Information, $benchmark)
    {
        $this->benchmark = $benchmark;
        $this->Information = $Information;
        return $this->recipe();
    }
    
    
    private function recipe()
    {
        $helper = new PHPhelpers();
        $benchmark = [];

        $company = array_reverse($this->Information['report']['Financial']['FinancialIndicators']['ReturnsOnAssets'])[0]['amount']; 

        $benchmark['min'] = $helper->chart_porcentage_formatter($this->Information['UFfactor'] * $this->benchmark['min_roi']) ;
        $benchmark['max'] = $helper->chart_porcentage_formatter($this->Information['UFfactor'] * $this->benchmark['max_roi']) ;
        $benchmark['avg'] = $helper->chart_porcentage_formatter($this->Information['UFfactor'] * $this->benchmark['avg_roi']) ;
        $benchmark['stdv'] = $helper->chart_porcentage_formatter($this->Information['UFfactor'] * $this->benchmark['stddev_roi']) ;

        $benchmark['company'] = $company;
            
        return $benchmark;
        
    }
}