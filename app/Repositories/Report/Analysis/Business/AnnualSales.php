<?php
namespace acempresarial\Repositories\Report\Analysis\Business;
class AnnualSales
{
    public function get($CTE)
    {
        return $this->recipe($CTE);
    }
    
    
    private function recipe($CTE)
    {
        $F22s = $CTE->f22s;
        $sales = [];
       
        foreach ($F22s as $F22) {
            $sales[] =
                [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=>$F22->C628
                ] ;
          
        }
        
        return $sales;
    }
}
