<?php 
namespace acempresarial\Repositories\Report\Analysis\Business\Charts\salesLast12Month;


class Months
{
	private $analysis; 
	public function get($analysis)
	{
		$this->analysis = $analysis;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	
		$result = [];
	
		$months = $this->analysis['MonthsArray'];
			
		foreach ($months as $month) {
			array_push($result,$month['strings']['month']);
		}	

		$result = array_reverse($result );
		
		$result = collect($result)->toJson();

	    return $result;
	}
	
}