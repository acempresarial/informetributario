<?php 
namespace acempresarial\Repositories\Report\Analysis\Business\Charts\salesLast12Month;
use acempresarial\Helpers\PHPhelpers;

class Sales
{
	private $analysis;
	public function get($analysis)
	{
		$this->analysis = $analysis;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	
		$helper = new PHPhelpers();
		$result = [];
		
	
		$sales = $this->analysis['SalesMonths'];
			
		foreach ($sales as $sale => $value) {		
			array_push($result,$helper->chart_millions_formatter($value));
		}	

		$result = array_reverse($result );
		
		$result = collect($result)->toJson();
	
	    return $result;
	}
	
}