<?php 

namespace acempresarial\Repositories\Report\Analysis\Business\Charts\salesLast12Month;

use acempresarial\Repositories\Report\Analysis\Business\Charts\salesLast12Month\Months;
use acempresarial\Repositories\Report\Analysis\Business\Charts\salesLast12Month\Sales;
use acempresarial\Repositories\Report\Analysis\Business\Charts\salesLast12Month\Porcentages;

/**
* 
*/
class salesLast12MonthLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the General information page
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($analysis)
	{				
		$chart['labels'] = (new Months)->get($analysis);	
		$chart['sales'] = (new Sales)->get($analysis);	
		$chart['porcentages'] = (new Porcentages)->get($analysis);	
		return $chart;
	}
}