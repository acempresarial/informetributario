<?php 
namespace acempresarial\Repositories\Report\Analysis\Business\Charts\salesLast3Years;
use acempresarial\Helpers\PHPhelpers;

class Sales
{
	private $analysis;
	public function get($analysis)
	{
		$this->analysis = $analysis;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	
		$helper = new PHPhelpers();
		$result = [];
	
		$sales = $this->analysis['AnnualSales'];
	
		foreach ($sales as $sale => $value) {		
			array_push($result,$helper->chart_millions_formatter($value['amount']));
		}	

		$result = array_reverse($result );
		
		$result = collect($result)->toJson();
	
	    return $result;
	}
	
}