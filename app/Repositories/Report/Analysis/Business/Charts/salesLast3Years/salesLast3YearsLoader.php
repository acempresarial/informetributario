<?php 

namespace acempresarial\Repositories\Report\Analysis\Business\Charts\salesLast3Years;

use acempresarial\Repositories\Report\Analysis\Business\Charts\salesLast3Years\Years;
use acempresarial\Repositories\Report\Analysis\Business\Charts\salesLast3Years\Sales;


/**
* 
*/
class salesLast3YearsLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the General information page
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($analysis)
	{				

		$chart['labels'] = (new Years)->get($analysis);	
		$chart['sales'] = (new Sales)->get($analysis);			
		return $chart;
	}
}