<?php
namespace acempresarial\Repositories\Report\Analysis\Business;

use acempresarial\Repositories\Report\Analysis\Business\SalesLast12MonthsArray;
use acempresarial\Helpers\PHPhelpers;

class MaxSale
{
    public function get($CTE)
    {
        return $this->recipe($CTE);
    }
    
    
    private function recipe($CTE)
    {
        $helper = new PHPhelpers();
        $sales = (new SalesLast12MonthsArray)->get($CTE);
        $collection = collect($sales);
        $max_sale =$collection->max();

        $sale['amount'] = $max_sale;
        $sale['formatted']['millions'] = $helper->millions_formatter($max_sale);     

        $sale = collect($sale);        

        return $sale;
    }
}
