<?php 
namespace acempresarial\Repositories\Report\Analysis\Business;


use acempresarial\Models\F29;
use acempresarial\Repositories\Report\Analysis\Business\Last12MonthsArray;
use acempresarial\Repositories\Report\Analysis\Business\SalesLast12MonthsArray;
use DB;

class MonthlySalesUf 
{
	
	public function get($CTE)
	{
		return $this->recipe($CTE);
	}
	
	
	private function recipe($CTE)
	{	
		$months = (new Last12MonthsArray)->get($CTE);
		$sales = (new SalesLast12MonthsArray)->get($CTE);
		$ufs = [];

		$j=0;
			foreach ($months as $month) {
				$date = substr($month['date'], 0, 10);
				$uf = DB::table('ufs')->where('date', $month['date'])->first();
				$ufs[$j] = $uf->value;
				$j++;
			}

		$SalesUF = [];
		for ($q=0; $q < count($months) ; $q++) { 
			$SalesUF[$q]=$sales[$q]/$ufs[$q];
		}
		return $SalesUF;
	}
	
}