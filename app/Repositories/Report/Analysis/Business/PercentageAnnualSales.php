<?php
namespace acempresarial\Repositories\Report\Analysis\Business;
use acempresarial\Repositories\Report\Analysis\Business\AnnualSales;
use acempresarial\Helpers\PHPhelpers;
class PercentageAnnualSales
{
    public function get($CTE)
    {
        return $this->recipe($CTE);
    }
    
    
    private function recipe($CTE)
    {

        $helper = new PHPhelpers();       
        //En esta segunda opcion se calcula el porcentaje de aumento en la venta para un año con respecto al año anterior, para el ultimo año(año mas antiguo) se define su aumento como 0
        $sales = (new AnnualSales)->get($CTE);
        $porcentages = [];

        foreach ($sales as $key => $sale) {
           if( isset($sales[$key+1]['amount']) && $sales[$key+1]['amount'] !=0)
           {       
                $case ="disminuyeron";   
                if($sale['amount']>$sales[$key+1]['amount'])
                {
                    $case = "aumentaron";
                }
                $porcentages[]=
                [
                    'years' => $sale['year']."-".$sales[$key+1]['year'],
                    'base_year'=>$sales[$key+1]['year'],
                    'base_amount'=>$helper->millions_formatter($sales[$key+1]['amount']) ,
                    'compare_amount'=>$helper->millions_formatter($sale['amount']),
                    'compare_year'=>$sale['year'],
                    'porcentage'=>($sale['amount']/$sales[$key+1]['amount'])-1,
                    'formatted_porcentage'=>$helper->porcentage_formatter(($sale['amount']/$sales[$key+1]['amount'])-1),
                    'case'=>$case
                ];
           }
          

        }        
       
       
        return $porcentages;
    }
}
