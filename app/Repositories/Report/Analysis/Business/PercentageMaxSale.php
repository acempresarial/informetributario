<?php 
namespace acempresarial\Repositories\Report\Analysis\Business;


use acempresarial\Repositories\Report\Analysis\Business\SalesPercentage;
use acempresarial\Helpers\PHPhelpers;

class PercentageMaxSale 
{
	
	public function get($CTE)
	{
		return $this->recipe($CTE);
	}
	
	
	private function recipe($CTE)
	{	
		$helper = new PHPhelpers();
		
		$sales = (new SalesPercentage)->get($CTE);
		$collection = collect($sales);
		$result =$helper->porcentage_formatter($collection->max()); 
		return $result;
	}
	
}