<?php 
namespace acempresarial\Repositories\Report\Analysis\Business;


use acempresarial\Models\F29;
use acempresarial\Repositories\Report\Analysis\Business\Last12MonthsArray;

class SalesLast12MonthsArray 
{
	
	public function get($CTE)
	{
		return $this->recipe($CTE);
	}
	
	
	private function recipe($CTE)
	{	
		$id= $CTE->id;
		$months = (new Last12MonthsArray)->get($CTE);
		
		$sales = [];	
		
		foreach($months as $month)
		{
			$F29 = F29::where('cte_id', $id)->where('C15',$month['date'])->first();
			$sale =
				 $F29->C020 + $F29->C142 + 
				 ( ( $F29->C502 + $F29->C111 )  / 0.19 ) + 
				 ( $F29->C717 / 0.19 ) - ( ( $F29->C510 + $F29->C709 ) / 0.19 );
			$sales[] = $sale;				
		}


		return $sales;
	}
	
}