<?php
namespace acempresarial\Repositories\Report\Analysis\Business;
use acempresarial\Repositories\Report\Analysis\Business\AnnualSales;
class SalesLastYear
{
    public function get($CTE)
    {
        return $this->recipe($CTE);
    }
    
    
    private function recipe($CTE)
    {

        $years= (new AnnualSales)->get($CTE);            
        $result = $years[0];
        return $result;
    }
}
