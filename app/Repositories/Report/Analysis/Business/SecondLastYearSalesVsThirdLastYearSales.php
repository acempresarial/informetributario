<?php
namespace acempresarial\Repositories\Report\Analysis\Business;
use acempresarial\Repositories\Report\Analysis\Business\SalesThirdLastYear;
use acempresarial\Repositories\Report\Analysis\Business\SalesSecondLastYear;    
class SecondLastYearSalesVsThirdLastYearSales
{
    public function get($CTE)
    {
        return $this->recipe($CTE);
    }
    
    
    private function recipe($CTE)
    {
        $thirdlast= (new SalesThirdLastYear)->get($CTE)['amount'];
        $secondlast = (new SalesSecondLastYear)->get($CTE)['amount'];

          if($thirdlast != 0)
        {
            $value = ($secondlast-$thirdlast)/$thirdlast;
            return $value;
        }
        
    }
}
