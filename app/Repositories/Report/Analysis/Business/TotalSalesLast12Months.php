<?php
namespace acempresarial\Repositories\Report\Analysis\Business;

use acempresarial\Repositories\Report\Analysis\Business\SalesLast12MonthsArray;

class TotalSalesLast12Months
{
    public function get($CTE)
    {
        return $this->recipe($CTE);
    }
    
    
    private function recipe($CTE)
    {
        $sales = (new SalesLast12MonthsArray)->get($CTE);
        $suma = 0;
        foreach ($sales as $venta) {
            $suma = $suma + $venta;
        }

        return $suma;
    }
}
