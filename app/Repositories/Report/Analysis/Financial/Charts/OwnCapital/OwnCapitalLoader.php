<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\Charts\OwnCapital;

use acempresarial\Repositories\Report\Analysis\Financial\Charts\OwnCapital\OwnCapital;

/**
* 
*/
class OwnCapitalLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the Business Charts section
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($analysis)
	{		
		$charts = array();			
	
		$charts['OwnCapital'] = (new OwnCapital)->get($analysis);
		
		return $charts;
	}
}