<?php 
namespace acempresarial\Repositories\Report\Analysis\Financial\Charts\TotalAssets;

use acempresarial\Helpers\PHPhelpers;

class TotalAssets
{
	private $analysis;
	public function get($analysis)
	{
		$this->analysis = $analysis;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	

		$helper = new PHPhelpers();
		$result = [];
		
		$assets = $this->analysis['TotalAssets']['AnnualAssets']['Pesos'];
		
		foreach ($assets as $assets) {
			
			$result[] =$helper->chart_millions_formatter($assets['amount']) ;

		}
		
		$result = array_reverse($result );
		
		$result = json_encode($result);
		
	    return $result;
	}
	
}