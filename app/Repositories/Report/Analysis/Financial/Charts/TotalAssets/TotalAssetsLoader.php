<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\Charts\TotalAssets;

use acempresarial\Repositories\Report\Analysis\Financial\Charts\TotalAssets\TotalAssets;
use acempresarial\Repositories\Report\Analysis\Financial\Charts\TotalAssets\Years;
/**
* 
*/
class TotalAssetsLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the Business Charts section
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($analysis)
	{		
		$charts = array();			
		$charts['labels'] = (new Years)->get($analysis);
		$charts['TotalAssets'] = (new TotalAssets)->get($analysis);
		
		return $charts;
	}
}