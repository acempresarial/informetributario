<?php 
namespace acempresarial\Repositories\Report\Analysis\Financial\Charts\TotalAssets;

use acempresarial\Helpers\PHPhelpers;

class Years
{
	private $analysis;
	public function get($analysis)
	{
		$this->analysis = $analysis;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	

		$helper = new PHPhelpers();
		$result = [];
		
		$assets = $this->analysis['TotalAssets']['Annual_Fixed_AssetsRelation'];
		
		foreach ($assets as $assets) {
			
			$result[] = $assets['year'];

		}
		
		$result = array_reverse($result );
		
		$result = json_encode($result);
		
	    return $result;
	}
	
}