<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\FinancialIndicators;

use acempresarial\Helpers\PHPhelpers;
/**
* 	
*/
class AssetsRotation 
{
	
	private $financialAnalysis;
    private $previousAnalysis;

    public function get($financialAnalysis,$previousAnalysis)
    {
        $this->financialAnalysis = $financialAnalysis;
        $this->previousAnalysis = $previousAnalysis;
        return $this->recipe();
    }

    /**
     * [recipe description]
     * @return [type] [description]
     */
    private function recipe()
    {    
        $helper = new PHPhelpers();
        $Sales = $this->previousAnalysis['Business']["AnnualSales"] ;
        $Assets =  $this->financialAnalysis["TotalAssets"]['AnnualAssets']['Pesos'];
        $rotation = [];  

        for ($i=0; $i < count($Assets); $i++) { 
               
                if($Assets[$i]['amount'] != 0)
                {
                    $rotation[] = 
                     [
                        'year'=> $Assets[$i]['year'],
                        'value'=>$helper->chart_decimal_formatter( $Sales[$i]['amount'] /  $Assets[$i]['amount'])
                     ];
                }
                
             }     
             $rotation = array_reverse($rotation);
        return $rotation;
    }
}