<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial;

use acempresarial\Repositories\Report\Analysis\Financial\TotalAssets\TotalAssetsLoader;
use acempresarial\Repositories\Report\Analysis\Financial\FixedAssets\FixedAssetsLoader;
use acempresarial\Repositories\Report\Analysis\Financial\OwnCapital\OwnCapitalLoader;
use acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements\IncomeStatementsLoader;
use acempresarial\Repositories\Report\Analysis\Financial\FinancialIndicators\FinancialIndicatorsLoader;
use acempresarial\Repositories\Report\Analysis\Financial\Charts\FinancialChartsLoader;

class FinancialLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the General information page
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($cte, $previousAnalysis)
	{
	
		$analysis = array();
		$analysis['TotalAssets'] = (new TotalAssetsLoader)->get($cte);
		$analysis['OwnCapital'] = (new OwnCapitalLoader)->get($cte);
		$analysis['IncomeStatement'] = (new IncomeStatementsLoader)->get($cte);
		$analysis['FinancialIndicators'] = (new FinancialIndicatorsLoader)->get($cte,$previousAnalysis,$analysis);
		$analysis['Charts'] = (new FinancialChartsLoader)->get($cte,$analysis);
		
		return $analysis;
	}
}