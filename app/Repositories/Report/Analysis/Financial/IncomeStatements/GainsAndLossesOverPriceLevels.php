<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements;

use acempresarial\Helpers\PHPhelpers;
/**
* 
*/
class GainsAndLossesOverPriceLevels
{
	
	private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }

    /**
     * [recipe description]
     * @return array [description]
     */
    private function recipe()
    {
         $helper = new  PHPhelpers();
    	$F22s = $this->CTE->f22s;
        $margin = [];       

        foreach ($F22s  as $F22) {
            $calculation = ($F22->C638 - $F22->C637 );
            $margin[] = 
                 [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=>$calculation ,
                    'chilean_curency_amount' => $helper->chilean_currency_formatter(($calculation)),
                    'millions_amount'=>$helper->millions_formatter($calculation),
                    'chart_millions_amount'=>$helper->chart_millions_formatter($calculation)
                 ];              
        }
        $margin = array_reverse($margin);
        return $margin;
    }
}