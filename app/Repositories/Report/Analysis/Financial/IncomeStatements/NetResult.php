<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements;

use acempresarial\Helpers\PHPhelpers;
/**
* 
*/
class NetResult
{
	
	private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }

    private function recipe()
    {
         $helper = new PHPhelpers();
    	$F22s = $this->CTE->f22s;
        $result = [];       

        foreach ($F22s  as $F22) {
            $calculation =  ( ($F22->C628 + $F22->C629) - $F22->C630 ) - ($F22->C631 + $F22->C632) - $F22->C633 -$F22->C635 - ($F22->C618 * 0.19);
            $result[] = 
                 [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=>$calculation,
                    'chilean_curency_amount' => $helper->chilean_currency_formatter(($calculation)),
                    'millions_amount'=>$helper->millions_formatter($calculation),
                    'chart_millions_amount'=>$helper->chart_millions_formatter($calculation)
                 ];        
        
        }
        $result = array_reverse($result);
        return $result;
    }
}