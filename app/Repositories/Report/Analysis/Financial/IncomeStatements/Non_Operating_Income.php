<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements;

use acempresarial\Helpers\PHPhelpers;
/**
* 
*/
class Non_Operating_Income 
{
	
	private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }

    private function recipe()
    {
         $helper = new PHPhelpers();
    	$F22s = $this->CTE->f22s;
        $incomes = [];       

        foreach ($F22s  as $F22) {
         
            $incomes[] = 
                 [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=>$F22->C651,
                    'chilean_curency_amount' => $helper->chilean_currency_formatter(($F22->C651)),
                    'millions_amount'=>$helper->millions_formatter($F22->C651),
                    'chart_millions_amount'=>$helper->chart_millions_formatter($F22->C651) 
                 ];        
           
        }
        $incomes = array_reverse($incomes);
        return $incomes;
    }
}