<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\IncomeStatements;

use acempresarial\Helpers\PHPhelpers;
/**
* 
*/
class RentTax
{
	
	private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }

    private function recipe()
    {
         $helper = new PHPhelpers();
    	$F22s = $this->CTE->f22s;
        $tax = [];       

        foreach ($F22s  as $F22) {
         $calculation =$F22->C635 * 0.19 ;
            $tax[] = 
                 [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=>$calculation ,
                    'chilean_curency_amount' => $helper->chilean_currency_formatter(($calculation)),
                    'millions_amount'=>$helper->millions_formatter($calculation),
                    'chart_millions_amount'=>$helper->chart_millions_formatter($calculation)
                 ];      

        }
        $tax = array_reverse($tax);
        return $tax;
    }
}