<?php 
namespace acempresarial\Repositories\Report\Analysis\Financial\OwnCapital;

use acempresarial\Helpers\PHPhelpers;
/**
* 	
*/
class OwnCapital 
{
	
	private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }

    private function recipe()
    {
        $helper = new PHPhelpers();
        $F22s = $this->CTE->f22s;
        $ownCapital = [];       

        foreach ($F22s  as $F22) {
         
            $ownCapital[] = 
                 [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=>$F22->C645,
                    'chilean_curency_amount' => $helper->chilean_currency_formatter(($F22->C645)),
                    'millions_amount'=>$helper->millions_formatter($F22->C645),
                    'chart_millions_amount'=>$helper->chart_millions_formatter($F22->C645)
                 ];          
           
        }
       
        return $ownCapital;
    }
}