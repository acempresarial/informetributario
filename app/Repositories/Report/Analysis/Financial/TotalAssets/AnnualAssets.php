<?php

namespace acempresarial\Repositories\Report\Analysis\Financial\TotalAssets;

use DB;
use acempresarial\Helpers\PHPhelpers;
class AnnualAssets
{
    private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }
    
    /**
     * It calculates the total assests for each
     * year based on the CTE
     * @return [type] [description]
     */
    private function recipe()
    {
        $helper = new PHPhelpers();
        $F22s = $this->CTE->f22s;
        $assests = [];       

        foreach ($F22s  as $F22) {
         
            $assests['Pesos'][] = 
                 [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=>$F22->C122,
                    'chilean_curency_amount' => $helper->chilean_currency_formatter(($F22->C122)),
                    'millions_amount'=>$helper->millions_formatter($F22->C122),
                    'chart_millions_amount'=>$helper->chart_millions_formatter($F22->C122)
                 ];

            $uf = DB::table('ufs')->where('year', $F22->tax_year->format('Y'))->first();

            $assests['UF'][] = 
                [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=> ($F22->C122/$uf->value)                
                   
                ];
           
        }
    
        return $assests;
    }
}
