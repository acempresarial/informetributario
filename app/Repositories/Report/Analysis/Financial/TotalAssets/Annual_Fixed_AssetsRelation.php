<?php

namespace acempresarial\Repositories\Report\Analysis\Financial\TotalAssets;

class Annual_Fixed_AssetsRelation
{
    private $analysis;
    public function get($analysis)
    {
        $this->analysis = $analysis;
        return $this->recipe();
    }
    
    /**
     * Calculates the Annual and Fized Assets Relation
     * based on the previus analysis (Annual and Fized Assets)
     * @return [type] [description]
     */
    private function recipe()
    {       
        $AnnualAssets = $this->analysis['AnnualAssets'];
        $FixedAssets = $this->analysis['FixedAssets'];
       
        $relation = [];
    
        for ($i=0; $i < count($FixedAssets['Pesos']); $i++) { 
            
            if($AnnualAssets['Pesos'][$i]['amount'] != 0)
            {
                $relation[] = 
                    [
                        'year' => $FixedAssets['Pesos'][$i]['year'],
                        'amount' =>  ($FixedAssets['Pesos'][$i]['amount'] / $AnnualAssets['Pesos'][$i]['amount'] )
                    ];

            }
           
            
        }
        
        return $relation;

    }
}
