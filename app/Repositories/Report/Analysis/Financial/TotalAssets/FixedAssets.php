<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\TotalAssets;

use DB;
use acempresarial\Helpers\PHPhelpers;
/**
* 
*/
class FixedAssets 
{
	private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }

    private function recipe()
    {
        $helper = new PHPhelpers();
    	$F22s = $this->CTE->f22s;
        $fixed_assets = [];       

        foreach ($F22s  as $F22) {
         
            $fixed_assets['Pesos'][] = 
                 [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=>$F22->C647,
                    'chilean_curency_amount' => $helper->chilean_currency_formatter(($F22->C647)),
                    'millions_amount'=>$helper->millions_formatter($F22->C647),
                    'chart_millions_amount'=>$helper->chart_millions_formatter($F22->C647)
                 ];

            $uf = DB::table('ufs')->where('year', $F22->tax_year->format('Y'))->first();

            $fixed_assets['UF'][] = 
                [
                    'year'=>$F22->tax_year->format('Y'),
                    'amount'=> ($F22->C647/$uf->value)
                ];
           
        }
    
        return $fixed_assets;
    }

}

