<?php 

namespace acempresarial\Repositories\Report\Analysis\Financial\TotalAssets;

use acempresarial\Repositories\Report\Analysis\Financial\TotalAssets\AnnualAssets;
use acempresarial\Repositories\Report\Analysis\Financial\TotalAssets\Heritage;
use acempresarial\Repositories\Report\Analysis\Financial\TotalAssets\YearsArray;
use acempresarial\Repositories\Report\Analysis\Financial\TotalAssets\FundedPorcentage;
use acempresarial\Repositories\Report\Analysis\Financial\TotalAssets\Liabilities;
use acempresarial\Repositories\Report\Analysis\Financial\TotalAssets\FixedAssets;
use acempresarial\Repositories\Report\Analysis\Financial\TotalAssets\Annual_Fixed_AssetsRelation;


/**
* 
*/
class TotalAssetsLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the General information page
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($cte)
	{
	
		$result = array();
		$result['AnnualAssets'] = (new AnnualAssets)->get($cte);
		$result['Heritages'] = (new Heritage)->get($cte);
		$result['Years'] = (new YearsArray)->get($cte);
		$result['FundedPorcentage'] = (new FundedPorcentage)->get($result);
		$result['Liabilities'] = (new Liabilities)->get($cte);
		$result['FixedAssets'] = (new FixedAssets)->get($cte);
		$result['Annual_Fixed_AssetsRelation'] = (new Annual_Fixed_AssetsRelation)->get($result);	


		return $result;
	}
}