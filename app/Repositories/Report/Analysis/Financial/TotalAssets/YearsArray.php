<?php

namespace acempresarial\Repositories\Report\Analysis\Financial\TotalAssets;
use acempresarial\Repositories\Report\Analysis\Business\Last3Years;
class YearsArray
{
    private $CTE;
    public function get($CTE)
    {
        $this->CTE=$CTE;
        return $this->recipe();
    }
    
    
    private function recipe()
    {
        $years = (new Last3Years)->get($this->CTE);
        return $years;
    }
}
