<?php 

namespace acempresarial\Repositories\Report\Analysis\GeneralInformation;

use acempresarial\Repositories\Report\Analysis\GeneralInformation\ScoreCalculator;
use acempresarial\Repositories\Report\Analysis\GeneralInformation\SectorRetriever;
use acempresarial\Repositories\Report\Analysis\GeneralInformation\SegmentRetriever;
use acempresarial\Repositories\Report\Analysis\GeneralInformation\UfFactor;
use acempresarial\Repositories\Report\Analysis\GeneralInformation\LastDate;

use DB;
/**
* 
*/
class GeneralInformationLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the General information page
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($cte,$wizard)
	{
	
		$result = array();
		$result['score'] = (new ScoreCalculator)->get($cte);

		$result['activity']=  collect(DB::table('economic_activities')
			->where('id', $wizard['economic_activity'])->first())->toArray();
		$result['sector'] = (new SectorRetriever)->get($result['activity']['economic_sector_id']);
		$result['segment'] = (new SegmentRetriever)->get($result['score']);
		$result['UfFactor'] = (new UfFactor)->get($cte);
		$result['LastDate'] = (new LastDate)->get($cte);
		$result['CompanyName'] = $cte->company->name;
		
		return $result;
	}
}