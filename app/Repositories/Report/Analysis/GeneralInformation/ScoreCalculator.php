<?php 
namespace acempresarial\Repositories\Report\Analysis\GeneralInformation;

use DB;
/**
* El puntaje determina el segmento al que pertenece una 
* empresa, se utiliza el último formulario F22 cargado y 
* la UF del periodo anual correspondiente.
*/

class ScoreCalculator 
{
	private $cte;
	/**
	 * [get description]
	 * @param  [type] $CTE [description]
	 * @return [type]      [description]
	 */
	public function get($CTE)
	{
		$this->cte = $CTE;
		return $this->handle();
	}
	
	
	/**
	 * [recipe description]
	 * @param  [type] $CTE [description]
	 * @return [type]      [description]
	 */
	private function handle()
	{
		$sum = 0;
		$last_f22 = $this->cte->f22s->first();
		$year= $last_f22->tax_year->format('Y');
		$uff = DB::table('ufs')->where('year', $year)->orderBy('value','DESC')->first();

		$UF = $uff->value;

		$score = 
		100*( 
				(0.3 * ($last_f22->C628/$UF) / 88078)  +
				(0.2 * ($last_f22->C122/$UF) / 117437) + 
				(0.5 *
					( 
						0.046*( ($last_f22->C628/$UF) /1000)*2 + 
						0.001*(($last_f22->C122/$UF)/1000)*2+ 
						3.382  
					)/250
				)
			);
		return $score;
	}
	
}