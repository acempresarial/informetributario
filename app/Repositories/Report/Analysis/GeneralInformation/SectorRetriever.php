<?php 
namespace acempresarial\Repositories\Report\Analysis\GeneralInformation;

use acempresarial\Models\EconomicSector;

/**
* El puntaje determina el segmento al que pertenece una 
* empresa, se utiliza el último formulario F22 cargado y 
* la UF del periodo anual correspondiente.
*/
class SectorRetriever 
{
	private $economic_sector;
	/**
	 * [get description]
	 * @return [type] [description]
	 */
	public function get($sector_id)
	{
		$this->economic_sector =$sector_id;
		return $this->handle();
	}
	
	/**
	 * [handle description]
	 * @return [type] [description]
	 */
	private function handle()
	{		
		$result = array();
		$sector = EconomicSector::findOrFail($this->economic_sector);
		$result['name']=$sector->name;
		$result['code']=$sector->code;
		return $result;
	}
	
}