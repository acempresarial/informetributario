<?php 
namespace acempresarial\Repositories\Report\Analysis\GeneralInformation;

use acempresarial\Models\EconomicSector;
use DB;

/**
* Función que a partir del PUNTAJE calculado obtiene el SEGMENTO
*/
class UfFactor
{
	
	private $cte;

	/**
	 * [get description]
	 * @param  [type] $score [description]
	 * @return [type]        [description]
	 */
	public function get($cte)
	{
		$this->cte = $cte;
		return $this->handle();
	}

	/**
	 * [handle description]
	 * @return [type] [description]
	 */
	private function handle()
	{
		$last_f22 = $this->cte->f22s->first();
		$year= $last_f22->tax_year->format('Y');

		$uf_current = DB::table('ufs')->where('year', $year)->orderBy('value','DESC')->first()->value;
		
		$uf_2004 = DB::table('ufs')->where('year', 2004)->orderBy('value','DESC')->first()->value;

		$factor = $uf_current /  $uf_2004;

		return $factor ;
	}
	
}