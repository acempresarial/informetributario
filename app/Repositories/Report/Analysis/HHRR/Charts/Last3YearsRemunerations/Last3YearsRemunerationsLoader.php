<?php 

namespace acempresarial\Repositories\Report\Analysis\HHRR\Charts\Last3YearsRemunerations;

use acempresarial\Repositories\Report\Analysis\HHRR\Charts\Last3YearsRemunerations\Years;
use acempresarial\Repositories\Report\Analysis\HHRR\Charts\Last3YearsRemunerations\Remunerations;
/**
* 
*/
class Last3YearsRemunerationsLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the Remunerations page
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($analysis)
	{				

		$chart['labels'] = (new Years)->get($analysis);	
		$chart['remunerations'] = (new Remunerations)->get($analysis);			
		return $chart;
	}
}