<?php 
namespace acempresarial\Repositories\Report\Analysis\HHRR\Charts\Last3YearsRemunerations;

use acempresarial\Helpers\PHPhelpers;

class Years
{
	private $analysis;
	public function get($analysis)
	{
		$this->analysis = $analysis;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	

		$helper = new PHPhelpers();
		$result = [];
		
		$remunerations = $this->analysis['Remunerations']['Pesos'];
		
		foreach ($remunerations as $remuneration) {
			
			$result[] = $remuneration['year'];

		}
		
		$result = array_reverse($result );
		
		$result = json_encode($result);
	
	    return $result;
	}
	
}