<?php 
namespace acempresarial\Repositories\Report\Analysis\Operations;

use acempresarial\Repositories\Report\Analysis\Operations\DifferencesPurchasesSales;
use acempresarial\Helpers\PHPhelpers;
class AvgDifference 
{
	private $CTE;	
	public function get($CTE)
	{
		$this->CTE = $CTE;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	
		$helper = new PHPhelpers();
		$difference = (new DifferencesPurchasesSales)->get($this->CTE);
		$total=0;

		foreach ($difference as $diff) {
			$total= $total+$diff;
		}
		if(count($difference) != 0)
		{
			
			$avg=($total/count($difference));

			$avg= $helper->chart_millions_formatter($avg);
			return $avg;
		}
		
	}
	
}