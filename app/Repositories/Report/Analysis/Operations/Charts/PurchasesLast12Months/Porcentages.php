<?php 
namespace acempresarial\Repositories\Report\Analysis\Operations\Charts\PurchasesLast12Months;


class Porcentages
{
	private $analysis; 
	public function get($analysis)
	{
		$this->analysis = $analysis;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	
		$result = [];
		
		$months = $this->analysis['PercentagePurchases'];
			
		foreach ($months as $month) {
			array_push($result,$month['char_formatted_porcentage']);
		}	

		$result = array_reverse($result );
		
		$result = json_encode($result);
		
		return $result;
	}
	
}