<?php 
namespace acempresarial\Repositories\Report\Analysis\Operations\Charts\PurchasesSellsDifference;

use acempresarial\Repositories\Report\Analysis\Business\BusinessLoader;

class Difference
{
	private $analysis; 
	private $cte;
	public function get($cte,$analysis)
	{
		$this->analysis = $analysis;
		$this->cte = $cte;
		return $this->recipe();
	}
	
	/**
	 * [recipe description]
	 * @return [type] [description]
	 */
	private function recipe()
	{	

		$purchases = [];
		$difference = [];
		$Sells = json_decode((new BusinessLoader)
			->get($this->cte)['Charts']['salesLast12Month']['sales']);	
		
		//Deberiamos traer esta informacion de los calculos que ya estan hechos.	
		$months = $this->analysis['PurchasesLast12Months'];
			
		foreach ($months as $month) {
			array_push($purchases,$month['char_millions_amount']);
		}	

		$purchases = array_reverse($purchases );


		$amount = count($Sells);
		for ($i=0; $i < $amount; $i++) { 
			$difference[]= $Sells[$i] - $purchases[$i];
			
		}
		
		$result = json_encode($difference);

		return $result;
	}
	
}