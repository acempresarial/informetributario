<?php 
namespace acempresarial\Repositories\Report\Analysis\Operations;


use acempresarial\Repositories\Report\Analysis\Operations\MonthlySales;
use acempresarial\Repositories\Report\Analysis\Operations\PurchasesLast12MonthsArray;

class DifferencesPurchasesSales 
{
	private $CTE;	
	public function get($CTE)
	{
		$this->CTE = $CTE;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	
		$sales = (new MonthlySales)->get($this->CTE);
		$purchases = (new PurchasesLast12MonthsArray)->get($this->CTE);
		$array = [];
		for ($i=0; $i < count($purchases); $i++) { 
			$array[$i]=$sales[$i]-$purchases[$i]['amount'];
		}
		return $array;
	}
	
}