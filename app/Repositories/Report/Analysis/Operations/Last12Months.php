<?php
namespace acempresarial\Repositories\Report\Analysis\Operations;


use acempresarial\Repositories\Report\Analysis\Business\Last12MonthsArray;

class Last12Months
{
    private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }
    
    
    private function recipe()
    {
        $result = (new Last12MonthsArray)->get($this->CTE);
        return $result;
    }
}
