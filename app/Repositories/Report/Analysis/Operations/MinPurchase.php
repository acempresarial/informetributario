<?php
namespace acempresarial\Repositories\Report\Analysis\Operations;

use acempresarial\Repositories\Report\Analysis\Operations\PurchasesLast12MonthsArray;

class MinPurchase
{
    private $CTE;
    public function get($CTE)
    {
        $this->CTE = $CTE;
        return $this->recipe();
    }    
    
    private function recipe()
    {
        $porcentages = collect((new PercentageMonthlyPurchases)->get($this->CTE));
        $purchases = collect((new PurchasesLast12MonthsArray)->get($this->CTE));
            
        $min = $purchases->min('amount');       
           

        foreach ($purchases as $purchase) {
            if($purchase['amount'] == $min)
            {  
                $purchase['porcentage'] = $porcentages->where('month', $purchase['month'])->first();                
                return $purchase;
            }
        }

     
    }
}
