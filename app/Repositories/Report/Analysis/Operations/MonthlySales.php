<?php 
namespace acempresarial\Repositories\Report\Analysis\Operations;


use acempresarial\Repositories\Report\Analysis\Business\SalesLast12MonthsArray;

class MonthlySales 
{
	private $CTE;	
	public function get($CTE)
	{
		$this->CTE = $CTE;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	
		$sales = (new SalesLast12MonthsArray)->get($this->CTE);
		return $sales;
	}
	
}