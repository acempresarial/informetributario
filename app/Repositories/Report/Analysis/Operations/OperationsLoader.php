<?php 

namespace acempresarial\Repositories\Report\Analysis\Operations;

use acempresarial\Repositories\Report\Analysis\Operations\PurchasesLast12MonthsArray;
use acempresarial\Repositories\Report\Analysis\Operations\Last12Months;
use acempresarial\Repositories\Report\Analysis\Operations\TotalPurchasesLast12Months;
use acempresarial\Repositories\Report\Analysis\Operations\PercentageMonthlyPurchases;
use acempresarial\Repositories\Report\Analysis\Operations\MensualPurchasesUf;
use acempresarial\Repositories\Report\Analysis\Operations\TotalPurchasesUf;
use acempresarial\Repositories\Report\Analysis\Operations\MonthlySales;
use acempresarial\Repositories\Report\Analysis\Operations\DifferencesPurchasesSales;
use acempresarial\Repositories\Report\Analysis\Operations\AvgDifference;
use acempresarial\Repositories\Report\Analysis\Operations\UpperLimit;
use acempresarial\Repositories\Report\Analysis\Operations\LowerLimit;
use acempresarial\Repositories\Report\Analysis\Operations\MonthMinDifference;
use acempresarial\Repositories\Report\Analysis\Operations\MaxPurchase;
use acempresarial\Repositories\Report\Analysis\Operations\MinPurchase;
use acempresarial\Repositories\Report\Analysis\Operations\Charts\OperationsChartsLoader;


/**
* 
*/
class OperationsLoader
{	
	/**
	 * This function takes the CTE and evals all the different
	 * calculations for the Operations page
	 * @param  [type] $CTE [description]
	 * @return array      caculation
	 */
	public function get($cte)
	{
	
		$result = array();
		$result['PurchasesLast12Months'] = (new PurchasesLast12MonthsArray)->get($cte);
		$result['Months'] = (new Last12Months)->get($cte);
		$result['TotalPurchases'] = (new TotalPurchasesLast12Months)->get($cte);
		$result['PercentagePurchases'] = (new PercentageMonthlyPurchases)->get($cte);
		$result['MensualPurchasesUf'] = (new MensualPurchasesUf)->get($cte);
		$result['TotalPurchasesUf'] = (new TotalPurchasesUf)->get($cte);
		$result['Sales'] = (new MonthlySales)->get($cte);
		$result['MonthlyDifference'] = (new DifferencesPurchasesSales)->get($cte);
		$result['AvgDifference'] = (new AvgDifference)->get($cte);
		$result['UpperLimit'] = (new UpperLimit)->get($cte);
		$result['LowerLimit'] = (new LowerLimit)->get($cte);
		$result['MonthMinDifference'] = (new MonthMinDifference)->get($cte);
		$result['MaxPurchase'] = (new MaxPurchase)->get($cte);
		$result['MinPurchase'] = (new MinPurchase)->get($cte);
		$result['Charts'] = (new OperationsChartsLoader)->get($cte, $result);
		

		return $result;
	}
}