<?php 
namespace acempresarial\Repositories\Report\Analysis\Operations;


use acempresarial\Repositories\Report\Analysis\Operations\MensualPurchasesUf;

class TotalPurchasesUf 
{
	private $CTE;	
	public function get($CTE)
	{
		$this->CTE = $CTE;
		return $this->recipe();
	}
	
	
	private function recipe()
	{	
		$purchases = (new MensualPurchasesUf)->get($this->CTE);
		$suma = 0;
		foreach ($purchases as $compra) {
			$suma = $suma + $compra;
		}
		return $suma;
	}
	
}