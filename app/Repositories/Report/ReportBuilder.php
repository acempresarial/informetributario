<?php 

namespace acempresarial\Repositories\Report;

use acempresarial\Repositories\Report\Analysis\AnalysisLoader;
use acempresarial\Repositories\Report\ConditionalTexts\ConditionalTextsLoader;

/**
* 		
*/
class ReportBuilder 
{
	/**
	 * Generates the report using the analysis and the 
	 * conditional text Loader Classes
	 * @param  array $wizard_fields [description]
	 * @return array                [description]
	 */
 	public function generate($wizard_fields)
 	{
 		$report = [];
 		$report['analysis'] = (new AnalysisLoader)->load($wizard_fields);
 	    $report['text'] = (new ConditionalTextsLoader)->load($report);
 	   
 	    return $report;
 	}
}