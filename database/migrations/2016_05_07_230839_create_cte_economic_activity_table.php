<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCteEconomicActivityTable extends Migration
{
   
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cte_economic_activity', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cte_id')->unsigned();
            $table->integer('economic_activity_id')->unsigned();
            $table->timestamps();

            $table->foreign('cte_id')->references('id')->on('ctes')
                    ->onDelete('cascade');
            $table->foreign('economic_activity_id')->references('id')->on('economic_activities')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cte_economic_activity');
    }
}
