<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('partners', function (Blueprint $table) {
            $table->increments('id');            
            $table->integer('cte_id')->unsigned();
            $table->string('name');
            $table->string('rut');
            $table->string('date');
            $table->timestamps();

            $table->foreign('cte_id')->references('id')->on('ctes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('partners');
    }
}
