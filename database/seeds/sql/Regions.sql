
INSERT INTO `regions` (`id`, `name`, `ISO_3166_2_CL`,`created_at`,`updated_at`) VALUES
(1, 'Tarapacá', 'CL-TA',now(),now()),
(2, 'Antofagasta', 'CL-AN',now(),now()),
(3, 'Atacama', 'CL-AT',now(),now()),
(4, 'Coquimbo', 'CL-CO',now(),now()),
(5, 'Valparaíso', 'CL-VS',now(),now()),
(6, 'Región del Libertador Gral. Bernardo O’Higgins', 'CL-LI',now(),now()),
(7, 'Región del Maule', 'CL-ML',now(),now()),
(8, 'Región del Biobío', 'CL-BI',now(),now()),
(9, 'Región de la Araucanía', 'CL-AR',now(),now()),
(10, 'Región de Los Lagos', 'CL-LL',now(),now()),
(11, 'Región Aisén del Gral. Carlos Ibáñez del Campo', 'CL-AI',now(),now()),
(12, 'Región de Magallanes y de la Antártica Chilena', 'CL-MA',now(),now()),
(13, 'Región Metropolitana de Santiago', 'CL-RM',now(),now()),
(14, 'Región de Los Ríos', 'CL-LR',now(),now()),
(15, 'Arica y Parinacota', 'CL-AP',now(),now());