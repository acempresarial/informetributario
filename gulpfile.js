var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */   

elixir(function(mix) {	
    mix.sass('app.scss')
    .styles(
            [
            '../node_modules/dropzone/dist/min/dropzone.min.css', 
            'almasaeed2010/adminlte/dist/css/AdminLTE.min.css',
            'almasaeed2010/adminlte/dist/css/skins/_all-skins.min.css',
            'almasaeed2010/adminlte/plugins/iCheck/square/blue.css',
            'almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.css'

            ], 'public/css/compiled/allVendor.css', 'vendor/'
        )
    .scripts(
            [   

                'almasaeed2010/adminlte/plugins/Jquery/Jquery-2.2.0.min.js',
                'almasaeed2010/adminlte/plugins/JqueryUI/jquery-ui.min.js',
            	'../node_modules/dropzone/dist/min/dropzone.min.js', 
            	'almasaeed2010/adminlte/plugins/slimScroll/jquery.slimscroll.min.js',
            	'almasaeed2010/adminlte/plugins/fastclick/fastclick.js',
            	'almasaeed2010/adminlte/dist/js/app.min.js',
            	'almasaeed2010/adminlte/dist/js/demo.js',
            	'almasaeed2010/adminlte/plugins/iCheck/icheck.min.js',
            	'almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js',
            	'almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js',
                '../other_vendor/globalize.min.js',
                '../node_modules/chart.js/dist/Chart.min.js', 
                '../other_vendor/dx.chartjs.js', 
           


            ], 'public/js/compiled/allVendor.js', 'vendor/'
        )    
    .version([
    		'public/css/compiled/allVendor.css',
            'public/js/compiled/allVendor.js',            
        ])
    mix.browserify('main.js');
});