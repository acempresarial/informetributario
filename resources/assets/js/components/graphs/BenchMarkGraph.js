export default
{
	template: `
		<div v-el:div  style="height: 300px;align-self: center;">					
		</div>
		`,

	props: ['minimum','maximum','average','company'],	

	ready(){
		var average = this.average;
        var company = this.company;
       

        let inferiorLimit = this.minimum;
          if(parseFloat(this.company)<=parseFloat(this.minimum))
          {
            inferiorLimit =this.company;
          }

        let superiorLimit = this.maximum;

        
          if(parseFloat(this.company)>=parseFloat(this.maximum))
          {
            
            superiorLimit =this.company;

          } 
        var minimum = inferiorLimit;
        var maximum = superiorLimit;         
             $(this.$els.div).dxCircularGauge({
                scale: {
                    startValue: inferiorLimit,
                    endValue: superiorLimit,
                    tickInterval: 10,
                    label: {
                        useRangeColors: true,
                         customizeText: function (arg) {
                            return arg.valueText + ' MM$';
                        }
                    }
                },  
                rangeContainer: {
                    palette: 'pastel',
                    ranges: [
                        { startValue: inferiorLimit, endValue: this.average * 0.8 ,color: "#BB626A"},
                        { startValue: this.average * 0.8, endValue: this.average *1.2, color: "#DAC599"},
                        { startValue: this.average *1.2, endValue: superiorLimit, color: "#70B3A1"},
                    ]
                },
                
                value: this.company,
                animation: {
                        easing: 'linear',
                        duration: 2500
                    },
                subvalues: [this.company,this.average,superiorLimit, inferiorLimit],
                tooltip: {
                    enabled: true,
                    customizeTooltip: function (arg) { 

                      if(arg.valueText == average)
                      {

                        return {
                            text: 'Promedio del Sector: MM$ ' +arg.valueText  
                           };                        

                      }  else if(arg.valueText == company){
                        return {
                            text: 'Su empresa MM$: ' +arg.valueText  
                           }; 
                      }else if(arg.valueText == maximum)
                      {
                         return {
                            text: 'Máximo del Sector: MM$' +arg.valueText  
                           }; 

                      }
                      else if(arg.valueText == minimum)
                      {
                         return {
                            text: 'Mínimo del Sector: MM$' +arg.valueText  
                           }; 

                      }                     
                    },
                    font: {
                        color: '#679EC5',
                        size: 22
                    }
                },
                size: {
                    height: 300,
                    width: 500
                 },
                subvalueIndicator: {
                    type: 'textCloud',
                    
                    color: '#f05b41',
                    text: {
                    font: {  size: 12 } ,
                    format: {
                     
                        precision: 0
                            },
                            customizeText: function (arg) {
                                        
                              if(arg.valueText == average)
                              {
                                return 'Avg: ' +arg.valueText;                                

                              } else if(arg.valueText == company){
                                return 'Ud: ' +arg.valueText; 
                                   
                              }else if(arg.valueText == maximum)
                              {
                                 return 'Max: ' +arg.valueText;

                              }
                              else if(arg.valueText == minimum)
                              {
                                 return 'Min: ' +arg.valueText ;                                    

                              }
                              else{
                                return 'Ud Lidera con: ' +company ;
                              }
                            }
                    }
                }
            })


        }

}