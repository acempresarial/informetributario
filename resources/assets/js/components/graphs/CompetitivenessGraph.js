import Chart from 'chart.js';

export default
{
	template: `
		<div>
			<canvas v-el:canvas style="height: 300px; width: 635px;" width="635" height="300"></canvas>			
		</div>
		`,

	props: ['labels','company','comparison','comparisonlabel','max'],	

	ready(){
		let data = {
    labels: this.labels,
    datasets: [
        {
            label: this.comparisonlabel,
            backgroundColor: "rgba(179,181,198,0.2)",
            borderColor: "rgba(179,181,198,1)",
            pointBackgroundColor: "rgba(179,181,198,1)",
            pointBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(179,181,198,1)",
            data: this.comparison
        },
        {
            label: "Mi Empresa",
            backgroundColor: "rgba(255,99,132,0.2)",
            borderColor: "rgba(255,99,132,1)",
            pointBackgroundColor: "rgba(255,99,132,1)",
            pointBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(255,99,132,1)",
            data: this.company
        }
        
        
    ]
};
		
		let myBarChart = new Chart(
			this.$els.canvas.getContext('2d'), 
		{
            type: 'radar',
            data: data,
            options: {
                scale: {
                  reverse: false, 
                   
                   pointDot:false,
                    ticks: {
                    beginAtZero: true,
                    min:0,
                    max:100,
                    stepSize:10
                 },
                           
                  
                  pointLabels: {
                    fontSize: 20,
                   
                  }
                }
            }

            
        }


		);

	}

}
