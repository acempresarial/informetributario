import Chart from 'chart.js';

export default
{
	template: `
		<div>
			<canvas v-el:canvas style="height: 130px; width: 635px;" width="635" height="300"></canvas>			
		</div>
		`,

	props: ['labels','values','legend'],	

	ready(){
		let data = {
        labels:this.labels,
        datasets: [{
            label: this.legend,
            data: this.values,
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)'             
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(54, 162, 235, 1)',
                
            ],
            borderWidth: 1
        }]
    };
		
		let myBarChart = new Chart(
			this.$els.canvas.getContext('2d'), 

            {
            type: 'bar',
            data: data,
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
            }	


		);

	}

}