import Chart from 'chart.js';

export default
{
	template: `
		<div>
			<canvas v-el:canvas style="height: 130px; width: 635px;" width="635" height="300"></canvas>			
		</div>
		`,


	props: ['labels','difference','upper','lower','avg'],	

	ready(){
		let data = {
            labels: this.labels,
            datasets: [   {
                label: "DIFERENCIA ENTRE COMPRAS Y VENTAS EN MM$",
                    type:'line',
                    data: this.difference,
                    fill: false,
                    borderColor: '#EC932F',
                    backgroundColor: '#EC932F',
                    pointBorderColor: '#EC932F',
                    pointBackgroundColor: '#EC932F',
                    pointHoverBackgroundColor: '#EC932F',
                    pointHoverBorderColor: '#EC932F',
                    pointRadius:8,
                    pointHoverRadius: 15,
                   yAxisID: 'y-axis-3'
            } ,{
                label: "LÍMITE SUPERIOR MM$",
                    type:'line',
                    data: [this.upper,this.upper,this.upper,this.upper,this.upper,this.upper,this.upper,this.upper,this.upper,this.upper,this.upper,this.upper],
                    fill: false,
                    borderColor: '#FF6961',
                    backgroundColor: '#FF6961',
                    pointBorderColor: '#FF6961',
                    pointBackgroundColor: '#FF6961',
                    pointHoverBackgroundColor: '#FF6961',
                    pointHoverBorderColor: '#FF6961',
                    pointRadius:3,
                    pointHoverRadius: 5,
                   yAxisID: 'y-axis-3'
            },
            {
                label: "LÍMITE INFERIOR MM$",
                    type:'line',
                    data: [this.lower,this.lower,this.lower,this.lower,this.lower,this.lower,this.lower,this.lower,this.lower,this.lower,this.lower,this.lower],
                    fill: false,
                    borderColor: '#779ECB',
                    backgroundColor: '#779ECB',
                    pointBorderColor: '#779ECB',
                    pointBackgroundColor: '#779ECB',
                    pointHoverBackgroundColor: '#779ECB',
                    pointHoverBorderColor: '#779ECB',
                    pointRadius:3,
                    pointHoverRadius: 5,
                   yAxisID: 'y-axis-3'
            },
            {
                label: "PROMEDIO MM$",
                    type:'line',
                    data: [this.avg,this.avg,this.avg,this.avg,this.avg,this.avg,this.avg,this.avg,this.avg,this.avg,this.avg,this.avg],
                    fill: false,
                    borderColor: '#CFCFC4',
                    backgroundColor: '#CFCFC4',
                    pointBorderColor: '#CFCFC4',
                    pointBackgroundColor: '#CFCFC4',
                    pointHoverBackgroundColor: '#CFCFC4',
                    pointHoverBorderColor: '#CFCFC4',
                    pointRadius:3,
                    pointHoverRadius: 5,
                   yAxisID: 'y-axis-3'
            }

            ]
        };
		
		let myBarChart = new Chart(
			this.$els.canvas.getContext('2d'), 
		{
                type: 'linear',
                data: data,
                options: {
                responsive: true,
                tooltips: {
                  mode: 'label'
              },
              elements: {
                line: {
                    fill: true
                }
            },
              scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    },
                    labels: {
                        show: true,
                    }
                }],
                yAxes: [{
                    type: "linear",
                    display: true,
                    position: "left",
                    id: "y-axis-3",
                    gridLines:{
                        display: false
                    },
                    labels: {
                        show:true,
                        
                    }
                }]
            }
            }

            }


		);

	}

}
