<?php
return [  

'C122' => 'Total Activo',
'C123' => 'Total Pasivo',
'C18' => 'Impuesto Primera Categoría sobre rentas efectivas. (Base Imponible)',
'C366' => 'Crédito por bienes físicos del activo inmovilizado del ejercicio',
'C628' => 'Ingresos Percibidos O Devengados',
'C629' => 'Intereses Percibidos O Devengados',
'C630' => 'Costo Directo de Bienes Y Servicios',
'C631' => 'Remuneraciones',
'C632' => 'Depreciación',
'C633' => 'Intereses Pagados O Adeudados',
'C635' => 'Otros Gastos Deduc. de Ingresos Brutos',
'C637' => 'Corrección Monetaria Saldo Deudor',
'C638' => 'Corrección Monetaria Saldo Acreedor',
'C643' => 'Renta Liquida Imponible O Perdida Tributaria',
'C645' => 'Capital Propio Tributario Positivo',
'C647' => 'Activo Inmovilizado',
'C651' => 'Otros Ingresos Percibidos O Devengados',
'C82' => 'Crédito por Gastos de Capacitación',
'C839' => 'Remanente de Crédito por bienes físicos del activo inmovilizado
proveniente de inversiones',
'C893' => 'C893',
'C894' => 'C894',
'C20' => 'C20'

];