<?php
return [  

    'C03' => 'RUT',
    'C062' => 'PPM NETO',
    'C111' => 'Boletas de Débito',
    'C15' => 'Fecha',
    'C110' => 'Cantidad de documentos boletas',
    'C020' => 'Exportaciones',
    'C142' => 'Ventas y/o Servicios Excentos o No Grabados del giro',
    'C502'=>'Débitos Facturas emitidas por ventas y servicios del giro',
    'C504' => 'Remanente Crédito Fiscal mes anterior',
    'C510' => 'Débito Notas de Crédito emitidas por Facturas asociadas al giro',
    'C514' => 'IVA por documentos electrónicos recibidos sin derecho a crédito',
    'C520' => 'Crédito Facturas recibidas del giro y Facturas de compra emitidas',
    'C525' => 'Crédito Facturas activo fijo',
    'C528' => 'Crédito Notas de Crédito recibidas',
    'C527' => 'Crédito Notas de Crédito recibidas',
    'C532' => 'Crédito Notas de Débito recibidas',
    'C535' => 'Crédito Declaraciones de Ingreso (DIN) importaciones del giro',
    'C547' => 'Total Determinado',
    'C553' => 'Crédito Declaraciones de Ingreso (DIN) importaciones activo fijo',
    'C709' => 'Débito Notas de Crédito emitidas por Vales de máquinas autorizadas por el Servicio',
    'C717' => 'Débito Facturas y Notas de Débitos por ventas y servicios que no son del giro (activo fijo y otros)',
    'C077' => 'Remanente de crédito fiscal para el período siguiente',
    'C089' => 'IMP. DETERM IVA DETERM.',
    'C115' => 'TASA PPM 1ra. CATEGORIA',
    'C151' => 'RET, TASAS DE 10% SOBRE LAS RENT.',
    'C503' => 'Cantidad de Facturas Emitidas',
    'C509' => 'Cantidad de documentos notas de creditos emitidas',
    'C511' => 'CRED. IVA por documentos electrónicos',
    'C515' => 'C515',
    'C519' => 'Cantidad de Documentos Facturas Recibidas del Giro',
    'C537' => 'Total Creditos',
    'C538' => 'Total Debitos',
    'C563' => 'Base Imponible',
    'C573' => 'REMANENTE ANT. CAMBIO SUJ. PER. SGTE.',
    'C587' => 'C587',
    'C595' => 'SUB TOTAL IMP. DETERMINADO ANVERSO',
    'C598' => 'ANTICIPO A IMPUTAR/CAMBIO DE SUJ.',
    'C723' => 'Total Credito Capacitacion a Imputar',
    'C724' => 'REM CRED CAPACITACION, PERIODO SIGUIENTE',
    'C07' => 'Folio'


    


];