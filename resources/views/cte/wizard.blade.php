@extends('layouts.app')
@section('content')
<section class="content-header">
	<h1>
	Creación de Informe Tributario
	<small>Escoja su Actividad Pricipal</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">CTE</a></li>
		<li class="active">Wizard</li>
	</ol>
</section>
<section class="content">
	<!-- Default box -->
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">Escoja la Actividad que genere el mayor porcentaje de ingresos para su empresa</h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal" action="/report/generate" method="POST">
			{{csrf_field()}}
			<div class="box-body">
				<div class="form-group">
					<label for="Report[name]" class="col-sm-2 control-label">Nombre Informe</label>
					<div class="col-sm-10">
						<input type="hidden" name="Report[cte_id]" value="{{$cte->id}}">
						<input type="text" class="form-control" name="Report[name]">
						
					</div>
				</div>
				<div class="form-group">
					<label for="Report[economic_activity]" class="col-sm-2 control-label">Actividad</label>
					<div class="col-sm-10">
						{{
						Form::select('Report[economic_activity]',
						$cte->economicActivitiesArray()
						, null,
						['placeholder' => 'Escoja una Actividad',
						'class'=>'form-control'
						]
						)
						}}
						
					</div>
				</div>
				
				
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<a href="/cte"class="btn btn-default">Volver</a>
				<button type="submit" class="btn btn-info pull-right">Generar Informe</button>
			</div>
			<!-- /.box-footer -->
		</form>
	</div>
</section>
@endsection