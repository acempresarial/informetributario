 <div class="panel box box-default">
    <div class="box-header with-border">
      <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">
          Ventas Anuales (Últimos 3 años)
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
      <div class="box-body">

      @forelse($report->data->analysis->Business->PercentageAnnualSales as $comparison)
        Las ventas del año {{$comparison->compare_year}} (<strong>MM$ {{$comparison->compare_amount}}</strong>) 
        <strong>{{$comparison->case}}</strong> 
        en un <strong>{{abs($comparison->formatted_porcentage)}}%</strong> 
        con respecto al año {{$comparison->base_year}} (<strong>MM$ {{$comparison->base_amount}}</strong>)
        <br>
      @empty
      @endforelse
    
      </div>
    </div>
  </div>