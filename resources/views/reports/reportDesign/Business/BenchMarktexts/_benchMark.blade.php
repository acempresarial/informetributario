<div class="panel box box-default">
  <div class="box-header with-border">
    <h4 class="box-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="" aria-expanded="true">
        BenchMark: Ventas.
      </a>
    </h4>
  </div>
  <div id="collapseThree" class="panel-collapse collapse" aria-expanded="true">
    <div class="box-body">
      {{$report->data->analysis->Benchmark->Sales->text}}      
    </div>
  </div>
</div>