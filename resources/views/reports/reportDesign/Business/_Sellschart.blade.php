<div class="col-md-12">   
          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Ventas Últimos 12 meses</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <div class="box-body">
              <div class="chart">     

                <last-12-months-graph 
                  :labels="{{$report->data->analysis->Business->Charts->salesLast12Month->labels}}"
                  :values="{{$report->data->analysis->Business->Charts->salesLast12Month->sales}}" 
                  :valuelabel="'Ventas en $MM'"
                  :porcentages="{{$report->data->analysis->Business->Charts->salesLast12Month->porcentages}}"
                  :porcentageslabel="'% del Total'">                    
                </last-12-months-graph>               

              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

  </div>
   