

          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Resumen de la empresa</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <div class="box-body">
              <div class="chart">    
               
              <competitiveness-graph
              :labels="{{json_encode($report->data->analysis->Benchmark->Competitiveness->labels)}}"
              :company="{{json_encode($report->data->analysis->Benchmark->Competitiveness->company)}}"
              :comparison="{{json_encode($report->data->analysis->Benchmark->Competitiveness->avg)}}"
              :max="{{json_encode($report->data->analysis->Benchmark->Competitiveness->max)}}"
              :comparisonlabel="'Promedio del Sector'">
              </competitiveness-graph>          

              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
