<div class="col-md-12">
<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Estado de Resultados Tributario</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
              <div class="table-responsive">
                <table class="table no-margin">
                  <th class="col-md-3"></th>
                  <th class="col-md-3"></th>
                  @forelse($report->data->analysis->Financial->IncomeStatement->Incomes as $income)
                  <th class="col-md-2" style="text-align: right">{{$income->year}}</th>
                  @empty
                  @endforelse
                  
                  <tbody>
                  <tr >
                    <td style="text-align: right"> </td>
                    <td style="text-align: left">(1) Ingresos</td>
                     @forelse($report->data->analysis->Financial->IncomeStatement->Incomes as $income)
                    <td style="text-align: right">$ {{$income->chilean_curency_amount}}</td>
                    @empty
                    @endforelse
                  </tr>
                  <tr >
                    <td style="text-align: right"> </td>
                    <td style="text-align: left">(2) Costos</td>                    
                    @forelse($report->data->analysis->Financial->IncomeStatement->Costs as $cost)
                      <td style="text-align: right">$ {{$cost->chilean_curency_amount}}</td>
                    @empty
                    @endforelse
                  </tr>
                   <tr >
                   <td style="text-align: right"><strong>(1) - (2) =</strong></td>
                    <td style="text-align: left"><strong>(3) Margen Bruto</strong></td>                    
                    @forelse($report->data->analysis->Financial->IncomeStatement->GrossMargin as $margin)
                      <td style="text-align: right"><strong>$ {{$margin->chilean_curency_amount}}</strong></td>
                    @empty
                    @endforelse
                  </tr>
                  <tr >
                   <td style="text-align: right"></td>
                    <td style="text-align: left">(4) Gastos de Administración y Venta</td>                    
                    @forelse($report->data->analysis->Financial->IncomeStatement->Administration_Sells_Expenses as $expense)
                      <td style="text-align: right">$ {{$expense->chilean_curency_amount}}</td>
                    @empty
                    @endforelse
                  </tr>
                    <tr >
                   <td style="text-align: right"><strong>(3) - (4) =</strong></td>
                    <td style="text-align: left"><strong>(5) Resultado Operacional</strong></td>                    
                    @forelse($report->data->analysis->Financial->IncomeStatement->OperationalResult as $result)
                      <td style="text-align: right"><strong>$ {{$result->chilean_curency_amount}}</strong></td>
                    @empty
                    @endforelse
                  </tr>
                   <tr >
                   <td style="text-align: right"></td>
                    <td style="text-align: left">(6) Gastos Financieros</td>                    
                    @forelse($report->data->analysis->Financial->IncomeStatement->FinancialExpenses as $expenses)
                      <td style="text-align: right">$ {{$expenses->chilean_curency_amount}}</td>
                    @empty
                    @endforelse
                  </tr>
                  <tr >
                   <td style="text-align: right"></td>
                    <td style="text-align: left">(7) Ingresos Fuera de Explotación</td>          
                          
                    @forelse($report->data->analysis->Financial->IncomeStatement->Non_Operating_Income as $incomes)
                      <td style="text-align: right">$ {{$incomes->chilean_curency_amount}}</td>
                    @empty
                    @endforelse
                  </tr>
                   <tr >
                   <td style="text-align: right"></td>
                    <td style="text-align: left">(8) Egresos Fuera de Explotación</td>          
                          
                    
                  </tr>
                  <tr >
                   <td style="text-align: right"></td>
                    <td style="text-align: left">(9) Utilidad o Pérdida por Corrección Monetaria</td>          
                          
                    @forelse($report->data->analysis->Financial->IncomeStatement->GainsAndLossesOverPriceLevels as $gains)
                      <td style="text-align: right">$ {{$gains->chilean_curency_amount}}</td>
                    @empty
                    @endforelse
                  </tr>
                   <tr >
                   <td style="text-align: right"><strong>(3) - (4) - (6) + (7) - (8) + (9) =</strong></td>
                    <td style="text-align: left"><strong>(10) Utilidad Antes de Impuesto</strong></td>          
                          
                    @forelse($report->data->analysis->Financial->IncomeStatement->PreTaxProfit as $pre)
                      <td style="text-align: right">$ {{$pre->chilean_curency_amount}}</td>
                    @empty
                    @endforelse
                  </tr>
                  <tr >
                   <td style="text-align: right"></td>
                    <td style="text-align: left">(11) Impuesto a la Renta</td>          
                          
                    @forelse($report->data->analysis->Financial->IncomeStatement->RentTax as $tax)
                      <td style="text-align: right">$ {{$tax->chilean_curency_amount}}</td>
                    @empty
                    @endforelse
                  </tr>
                  <tr >
                   <td style="text-align: right"><strong>(9) + (10) - (11) =</strong></td>
                    <td style="text-align: left"><strong>(12) Resultado Neto del Ejercicio</strong></td>          
                          
                    @forelse($report->data->analysis->Financial->IncomeStatement->NetResult as $netResult)
                      <td style="text-align: right"><strong>$ {{$netResult->chilean_curency_amount}}</strong></td>
                    @empty
                    @endforelse
                  </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix" style="display: block;">
           
            </div>
            <!-- /.box-footer -->
          </div>
          </div>