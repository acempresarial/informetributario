<div class="col-md-6">   
          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">BenchMark: Rentabilidad sobre las Ventas</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <div class="box-body">
                            
          
                <bench-mark-graph 
                  :minimum="{{$report->data->analysis->Benchmark->ReturnOnSales->min}}"
                  :maximum="{{$report->data->analysis->Benchmark->ReturnOnSales->max}}"
                  :average="{{$report->data->analysis->Benchmark->ReturnOnSales->avg}}"
                  :company="{{$report->data->analysis->Benchmark->ReturnOnSales->company}}">
                </bench-mark-graph>

                
            
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

  </div>  
