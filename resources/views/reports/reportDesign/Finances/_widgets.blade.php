<div class="row">
   
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h2>{{$report->data->analysis->Financial->TotalAssets->AnnualAssets->Pesos[0]->chilean_curency_amount}}</h2>

              <p>Activos Totales</p>
            </div>
            <div class="icon">
              <i class="fa fa-home"></i>
            </div>
           
          </div>
        </div>
     
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h2>$ {{$report->data->analysis->Financial->TotalAssets->FixedAssets->Pesos[0]->chilean_curency_amount}}</h2>

              <p>ACTIVOS FIJOS</p>
            </div>
            <div class="icon">
              <i class="fa fa-car"></i>
            </div>
            
          </div>
        </div>
       

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h2>$ {{$report->data->analysis->Financial->OwnCapital->OwnCapital[0]->chilean_curency_amount}}</h2>

              <p>CAPITAL PROPIO</p>
            </div>
            <div class="icon">
              <i class="fa fa-usd"></i>
            </div>
          </div>
          
        </div>

    
      </div>