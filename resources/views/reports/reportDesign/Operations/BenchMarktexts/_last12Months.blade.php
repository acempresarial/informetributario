<div class="panel box box-default">
    <div class="box-header with-border">
      <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#OperationsAcordion" href="#collapseOneOP" aria-expanded="false" class="collapsed">
          Compras Últimos 12 Meses
        </a>
      </h4>
    </div>
    <div id="collapseOneOP" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
      <div class="box-body">
       <p align="justify">
       Las compras totales de la empresa <strong> {{$report->data->analysis->GeneralInformation->CompanyName}} </strong> en el último periodo de 12 meses fueron <strong> MM${{$report->data->analysis->Operations->TotalPurchases->millions_amount}}</strong>.
       </p>
      <p align="justify">

       En los últimos 12 meses corridos, las mayores compras, por
        <strong> MM$ {{$report->data->analysis->Operations->MaxPurchase->millions_amount}}</strong>, se produjeron en el mes de 
        <strong>{{$report->data->analysis->Operations->MaxPurchase->string_month}}</strong>, esto equivale al 
        <strong>{{$report->data->analysis->Operations->MaxPurchase->porcentage->formatted_porcentage}}%</strong> 
        de las compras totales de los 12 últimos meses. En tanto, el mes de menores compras por
         <strong>MM$ {{$report->data->analysis->Operations->MinPurchase->millions_amount}}</strong>, fue 
         <strong>{{$report->data->analysis->Operations->MinPurchase->string_month}}</strong>, lo cual equivale al 
         <strong> {{$report->data->analysis->Operations->MinPurchase->porcentage->formatted_porcentage}}%</strong> 
          de las compras totales de este periodo. 

          <!-- 
          ¿Es correcto para su rubro que haya una gran diferencia en compras entre los meses de mayo de 2011 y diciembre de 2010 o sería conveniente hacer esfuerzos por distribuir mejor la compras a lo largo del año?
          -->
       </p>

              
      </div>
    </div>
</div>