<div class="row">

        <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-cart-arrow-down"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Compras Totales</span>
              <span class="info-box-number">{{$report->data->analysis->Operations->TotalPurchases->chilean_currency}}</span>

              <div class="progress">
                <div class="progress-bar" style="width: 0%"></div>
              </div>
                  <span class="progress-description">
                     Compras últimos 12 Meses
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
      </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
           <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">MES MAYORES COMPRAS</span>
              <span class="info-box-number">{{$report->data->analysis->Operations->MaxPurchase->string_month}}
                (MM$ {{$report->data->analysis->Operations->MaxPurchase->millions_amount}})</span>

              <div class="progress">
                <div class="progress-bar" style="width:0%"></div>
              </div>
                  <span class="progress-description">
                    {{$report->data->analysis->Operations->MaxPurchase->porcentage->formatted_porcentage}}
                    % Ventas Anuales  
                  </span>
            </div>


            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

         <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">MES MENORES COMPRAS</span>
              <span class="info-box-number">
                {{$report->data->analysis->Operations->MinPurchase->string_month}}
                (MM$ {{$report->data->analysis->Operations->MinPurchase->millions_amount}})</span>
              <div class="progress">
                <div class="progress-bar" style="width: 0%"></div>
              </div>
                  <span class="progress-description">
                    {{$report->data->analysis->Operations->MinPurchase->porcentage->formatted_porcentage}}% Ventas Anuales
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

    
      </div>