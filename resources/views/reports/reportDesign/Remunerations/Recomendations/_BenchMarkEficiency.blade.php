<div class="panel box box-default">
  <div class="box-header with-border">
    <h4 class="box-title">
      <a data-toggle="collapse" data-parent="#accordionRemunerations" href="#collapseFourRemunerations" class="" aria-expanded="true">
        BenchMark: Eficiencia en las Remuneraciones.
      </a>
    </h4>
  </div>
  <div id="collapseFourRemunerations" class="panel-collapse collapse" aria-expanded="true">
    <div class="box-body">
        {{$report->data->analysis->Benchmark->Productivity->text}}
    </div>
  </div>
</div>