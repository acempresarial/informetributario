<div class="panel box box-default">
  <div class="box-header with-border">
    <h4 class="box-title">
      <a data-toggle="collapse" data-parent="#accordionRemunerations" href="#collapseTWORemunerations" class="" aria-expanded="true">
        BenchMark: Remuneraciones.
      </a>
    </h4>
  </div>
  <div id="collapseTWORemunerations" class="panel-collapse collapse" aria-expanded="true">
    <div class="box-body">
        {{$report->data->analysis->Benchmark->Remunerations->text}}
    </div>
  </div>
</div>