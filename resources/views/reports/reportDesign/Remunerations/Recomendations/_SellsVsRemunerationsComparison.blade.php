<div class="panel box box-default">
  <div class="box-header with-border">
    <h4 class="box-title">
      <a data-toggle="collapse" data-parent="#accordionRemunerations" href="#collapseThreeRemunerations" class="" aria-expanded="true">
        Comparación de Remuneraciones remuneraciones y ventas.
      </a>
    </h4>
  </div>
  <div id="collapseThreeRemunerations" class="panel-collapse collapse" aria-expanded="true">
    <div class="box-body">

        @forelse($report->data->analysis->HHRR->RemunerationsOverSales as $comparison)
          El costo de las remuneraciones para el año <strong>{{$comparison->year}}</strong> representó un <strong>{{$comparison->amount}}%</strong> de las ventas totales.
          <br>
        @empty
        @endforelse

    </div>
  </div>
</div>