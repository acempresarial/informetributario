<div class="row">
        <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12">
           <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">REMUNERACIONES TOTALES</span>
              <span class="info-box-number">{{$report->data->analysis->HHRR->Remunerations->Pesos[0]->chilean_currency_amount}}</span>

              <div class="progress">
                <div class="progress-bar" style="width:0%"></div>
              </div>
                  <span class="progress-description">
                    Remuneraciones últimos 12 Meses
                  </span>
            </div>


            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        


    
 </div>