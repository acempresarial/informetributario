
    <b>ÁREA COMERCIAL: </b>

    <div class="box">
  
    <!-- /.box-header -->
    <div class="box-body">

        @include('reports/reportDesign/Business/_widgets')
        @include('reports/reportDesign/Business/_Sellschart')
        @include('reports/reportDesign/Business/_SellsLast3Yearschart')
        @include('reports/reportDesign/Business/_SellsBenchmark')
        @include('reports/reportDesign/Business/_text') 
    
    </div>    <!-- /.box-body -->
</div>
              