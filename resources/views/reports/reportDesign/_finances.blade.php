
    <b>ÁREA DE FINANZAS: </b>

    <div class="box">
  
    <!-- /.box-header -->
    <div class="box-body">
        @include('reports/reportDesign/Finances/_widgets') 
       	@include('reports/reportDesign/Finances/_TotalAssetsLast3years') 
       	@include('reports/reportDesign/Finances/_TotalFixedAssetsLast3Years') 
       	@include('reports/reportDesign/Finances/_TotalOwnCapital') 
   		@include('reports/reportDesign/Finances/_text') 
   		@include('reports/reportDesign/Finances/_IncomeStatements') 
      @include('reports/reportDesign/Finances/_FinaltialIndicators')
      @include('reports/reportDesign/Finances/_returnOnAssets')
     @include('reports/reportDesign/Finances/_returnOnSells')
      

      
    </div>    <!-- /.box-body -->
</div>
              