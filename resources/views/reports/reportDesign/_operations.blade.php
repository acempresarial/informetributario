
    <b>ÁREA DE OPERACIONES: </b>

    <div class="box">
  
    <!-- /.box-header -->
    <div class="box-body">

        @include('reports/reportDesign/Operations/_widgets') 
        @include('reports/reportDesign/Operations/_PurchasesLast12Months') 
   		@include('reports/reportDesign/Operations/_PurchasesVSSells') 
   		@include('reports/reportDesign/Operations/_PurchasesVsSellsDifference')    		
   		@include('reports/reportDesign/Operations/_text') 
    </div>    <!-- /.box-body -->
</div>
              