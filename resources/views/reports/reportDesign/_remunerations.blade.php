
    <b>REMUNERACIONES: </b>

    <div class="box">
  
    <!-- /.box-header -->
    <div class="box-body">

        @include('reports/reportDesign/Remunerations/_widgets')
        @include('reports/reportDesign/Remunerations/_SellsVsRemunerations')
        @include('reports/reportDesign/Remunerations/_RemunerationsLast3Years')
        @include('reports/reportDesign/Remunerations/_BenchMark')
        @include('reports/reportDesign/Remunerations/_BenchMarkEficiency') 
        @include('reports/reportDesign/Remunerations/_text')
    </div>    <!-- /.box-body -->
</div>
              