
    <b>Resumen: </b>

    <div class="box">
  
    <!-- /.box-header -->
    <div class="box-body">
    @include('reports/reportDesign/Competitiveness/_radar')




          <!-- BAR CHART -->
          <div class="box box-success collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Alcances del Informe</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <div class="box-body">
             <p align="justify">
    El presente Informe General se basa en la información proporcionada por su empresa al Servicio de Impuestos Internos (SII), en especial los Formularios 22 (Renta) y 29 (IVA). Por ende, el análisis depende de manera directa de la calidad y detalle de esta información. En las próximas semanas estará disponible un sistema de simulación que te permitirá conocer cuáles serían los resultados de tu empresa si algunos datos, como por ejemplo ventas, compras o remuneraciones tuvieran un valor distinto al que reporta tu CTE. 
    </p>
    <p align="justify">
    Para todas las comparaciones sectoriales (benchmarks), el presente Informe compara tu empresa con el 90% más representativo de empresas de similar tamaño a la tuya y del mismo rubro. Pueden producirse ciertas distorsiones en los resultados, si bajo un mismo RUT tu empresa tiene operaciones de similar importancia (volumen de ventas) en dos o más rubros. 
    </p>
    <p> <b>Sector:</b> {{$report->data->analysis->Scopes->sector->name}} </p>
    <p> <b>Ventas:</b> entre MM$XX y MM$YY </p>

    <p align="justify">
    Las comparaciones se realizan con los datos hasta el mes {{$report->data->analysis->GeneralInformation->LastDate->words}}, último año del Impuesto a la Renta (F22) contenido en tu Carpeta Tributaria Electrónica. Si puedes incluir en tu CTE un año más reciente te sugerimos reingresar tu CTE a la plataforma, así los resultados serán más actualizados. 
     <p align="justify">
    Te darás cuenta que en algunos casos, por ejemplo en Análisis Comercial, se muestran dos valores con pequeñas diferencias para un mismo año. Esto se debe a que, para mostrar de manera más exacta las variaciones se le ha descontado la inflación. De esta manera, percibirás cuál es la variación real de las ventas que no se deben a la variación por inflación (IPC).
    </p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    
    </div>

         <!-- /.box-body -->
</div>
              